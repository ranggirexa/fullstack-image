import { Sequelize } from "sequelize";

const db = new Sequelize('upload_db', 'postgres', 'postgres',{
	host:'localhost',
	dialect: "postgres"
});

 export default db;